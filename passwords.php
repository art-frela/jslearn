<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<title>Password generator</title>
	<link rel="stylesheet" href="css/style.css">
</head>
<?php 

// Символы, которые будут использоваться в пароле. 

$chars="!@#$%^&*())(*&^%$#@!asdfghjjkluiytreqazxswedcvfrtgbnhyujmkiolp1234567890QAZXSASDGDFHGFJKJDUTYSTREWEDCVFRTGBNHYUJMKIOLP!@#$%^&*()"; 

// Количество символов в пароле. 
if (!isset($_GET["passwordlength"])) {
	$max=10; 
} else {
	$max=$_GET["passwordlength"]; 
}


// Определяем количество символов в $chars 

$size=StrLen($chars)-1; 
$max2=$max;
// Определяем пустую переменную, в которую и будем записывать символы. 

$password=null; 

// Создаём пароль. 

    while($max--) 
    $password.=$chars[rand(0,$size)]; 


?>

<body>

	<div class="content">
		<div class="header">
			<a href="index.html">Главная</a>
			<a href="puzzle.html">Загадки</a>
			<a href="guess.html">Угадайка</a>
			<a href="passwords.php">Генератор паролей</a>
		</div>

		<h1>Генератор паролей</h1>

		<div class="center">
			<div class="generator">
				<p class="passwoprddescription">Вы видите сгенерированный пароль из 10 символов, если хотите получить новый другой
					длины выберете ее в поле ниже и нажмите "Создать пароль"</p>
				<p class="passwordvalue">
					<?php echo $password; ?>
				</p>
				<form method="GET">

					<select class="passwordlength" name="passwordlength">
						<?php
						for ($i = 7; $i <= $size-7; $i++) {
							if ($i==$max2) {
								
								echo "<option selected>$i</option>";
							} else {
								echo "<option>$i</option>";
							}
							
						}
					?>
					</select>
				
					<input type="submit" value="Создать пароль">
				</form>
			</div>

		</div>
	</div>
	<div class="footer">
		Copyright &copy; Artem Karpov
		<div>


</body>

</html>